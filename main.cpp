#include "widget.h"
#include "head.h"

#include <QTextCodec>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QTextCodec::setCodecForLocale(codec);

    Widget w;
    w.show();       //继承了Qwidget这个父类下的函数
    return a.exec();
}
