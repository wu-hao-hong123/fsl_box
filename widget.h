#ifndef WIDGET_H
#define WIDGET_H


#include <QTreeWidget>
#include <QTreeWidgetItem>

#include "serialport.h"
#include "head.h"
class serialPort;


class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    bool m_count = true;
    void ui_layout_init();
    void label_clear();
    void ui_label_display();

signals:
    void pushButton_open_serialTextChanged();      //声明一个信号

public slots:
    void pushButton_open_serial_Clicked();
    void pushButton_refresh_Clicked();
    void quit_Clicked();
    void fsl_pushButton_open_serial_Clicker_get_cco_address();
//    void serial_recv_TimeOut();



private:
    QWidget *page1;
    QWidget *page2;
    QWidget *page3;

    QPushButton *pushButton_open_serial;
    QPushButton *pushButton_refresh;
    QToolButton *Quit_botton;
    QLabel *labelString;
    QComboBox *qComboBox_serial;
    QComboBox *qComboBox_Baudrate;
    QLabel *label_node_num;
    QLabel *label_MAC_address;
    QLabel *label_device_info;
    QLabel *label_get_node_num;
    QLabel *label_get_MAC_address;
    QLabel *label_get_device_info;
//    QFrame *line_h;
//    QFrame *line_s;

    serialPort *SerialPort;

    QStackedWidget *m_stackedWidget; // 存放各个页面的容器
    QPushButton *m_button1;         // 导航栏上的按钮
    QPushButton *m_button2;
    QPushButton *m_button3;

    QPushButton *button_test;

    QSlider *slider;
    QLabel *slider_value_label;
    QCheckBox *checkBox;           //开关灯复选框
    short childIndex = -1;
    QLabel *label_brightness;
    QLabel *label_voltage;
    QLabel *label_electricity;
    QLabel *label_power;
    QLabel *label_leak_current;     //漏电流
    QLabel *label_power_factor;     //功率因数
    QLabel *label_temperature;
    QLabel *label_qingxie;
    QLabel *label_warning;
    QLabel *label_run_time;
    QLabel *label_electric_quantity;        //累计电量
    
    QLabel *label_voltage_value;
    QLabel *label_electricity_value;
    QLabel *label_power_value;
    QLabel *label_leak_current_value;       //漏电流
    QLabel *label_power_factor_value;       //功率因数
    QLabel *label_temperature_value;
    QLabel *label_qingxie_value;
    QLabel *label_warning_value;
    QLabel *label_run_time_value;
    QLabel *label_electric_quantity_value;        //累计电量

    QLabel *label_voltage_unit;
    QLabel *label_electricity_unit;
    QLabel *label_power_unit;
    QLabel *label_leak_current_unit;            //漏电流
    QLabel *label_power_factor_unit;            //功率因数
    QLabel *label_temperature_unit;
    QLabel *label_qingxie_unit;
    QLabel *label_run_time_unit;
    QLabel *label_electric_quantity_unit;      //累计电量


    QTreeWidget *treeWidget = NULL;
    QTreeWidgetItem *parentItem = NULL;
    QTreeWidgetItem *subItem = NULL;
public slots:
    void treeWidget_Clicked(QTreeWidgetItem *item, int);


};



#endif // WIDGET_H
