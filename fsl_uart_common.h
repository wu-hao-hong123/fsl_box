#ifndef FSL_UART_COMMON_H
#define FSL_UART_COMMON_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QByteArray>

class Fsl_end_common:public QObject
{
    Q_OBJECT
public:
    explicit Fsl_end_common(QObject *parent = NULL);
    ~Fsl_end_common();
};


#endif // FSL_UART_COMMON_H
