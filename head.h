#ifndef HEAD_H
#define HEAD_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>
#include <QDebug>
#include <QByteArray>
#include <QThread>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <QPushButton>
#include <QLabel>
#include <QTimer>
#include <QApplication>
#include <QToolButton>
#include <QScreen>
#include <QStyle>
#include <QComboBox>
#include <QPainter>
#include <QStackedWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QFrame>
#include <QThread>
#include <QCheckBox>
#include <QMessageBox>
#include "baima/include/gpio.h"

using namespace std;

#define fsl_serial_com2 "ttymxc1"
#define fsl_serial_com3 "ttymxc2"
#define fsl_serial_com7 "ttymxc5"


#define fsl_send_data_head  0x48
#define fsl_send_data_ctrl  0x00

#define OPEN_SERIAL_SUCCESS true
#define OPEN_SERIAL_FAILED false

typedef enum
{
    fsl_send_default_common_Query_device_nodes = 0x0020,
    fsl_send_default_common_get_cco_mac_adress = 0x0003,

}fsl_send_cco_defalut_common;

typedef enum
{
    fsl_send_cco_cus_CMD_common = 0x0120,
    fsl_send_cco_cus_query_device_info_func_code = 0x01,
    fsl_send_cco_cus_query_device_property_func_code = 0x08,
    fsl_send_cco_cus_write_sta_device_property = 0x07,
}fsl_send_cco_cus_func_code_common;

typedef enum
{
    fsl_recv_cco_cus_query_device_info_func_code = 0x81,
    fsl_recv_cco_cus_query_device_property_func_code = 0x88,

}fsl_recv_cco_cus_func_code_common;



typedef enum
{
    fsl_send_cmd_free = 0xff,
    fsl_sned_cmd_get_cco_mac_adress_flag = 0,
    fsl_sned_cmd_query_device_nodes_flag = 1,
    fsl_sned_cmd_query_device_info_flag = 2,
    fsl_sned_cmd_query_device_property_flag = 3,
    fsl_sned_cmd_write_appointed_sta_device_property_flag = 4,
}fsl_send_cmd_flag;

typedef enum
{
    fsl_recv_default_common_Query_device_nodes = 0x2000,
    fsl_recv_default_common_get_cco_mac_adress = 0x0300,

}fsl_recv_cco_defalut_common;

typedef enum
{
    fsl_switch_siid = 0x0001,
}plc_siid;

typedef enum
{
    fsl_OnOff_ciid = 0x0001,
    fsl_brightness_ciid = 0x0004,
}plc_ciid;

typedef enum
{
    m_int = 0x0001,
    m_bool = 2,
    m_string = 3,
    m_enum = 4,
    m_array = 5,
    m_qint16 = 6,
}data_type;

/* CCO设备信息 */
typedef struct Device_info
{
    QByteArray mac_address;
    QByteArray dev_address;
    QString string_dev_info;
    bool dev_type;              //true: cco  false: sta

}CCO_Device_Info;

/* STA设备属性 */
typedef struct Device_property
{
    bool light_switch;
    QByteArray mac_address;
    QByteArray dev_address;
    QByteArray brightness;
    QByteArray voltage_value;
    QByteArray electricity_value;
    QByteArray power_value;
    QByteArray leak_current_value;
    QByteArray power_factor_value;
    QByteArray temperature_value;
    QByteArray qingxie_value;
    QByteArray warning_value;
    QByteArray run_time_value;
    QByteArray electric_quantity_value;
}STA_Device;

#endif // HEAD_H
