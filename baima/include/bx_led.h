#ifndef __SHOW_LED__
#define __SHOW_LED__

#define NO_DEV_NODE  17
#define NO_RESPOND   18
#define SELECT_ERROR 19
#define RET_DATA_INVALID 20
#define INPUT_DATA_INVALID 21

struct led_request {
	char cmd_group;
	char cmd;
	char res;
	char reseved[2];
}  __attribute__((__packed__));

struct led_reply {
	char cmd_group;
	char cmd;
	char cmd_error;
	char reseved[2];
}  __attribute__((__packed__));

struct realtime_areas {
	char delete_area_num;
	char area_num;
	unsigned short area_data_len;
} __attribute__((__packed__));

struct area_data {
	char area_type;
	unsigned short area_x;
	unsigned short area_y;
	unsigned short area_width;
	unsigned short area_height;
	char dyn_area_loc;
	char line_sizes;
	char run_mode;
	unsigned short timeout;
	char reserved[3];
	char single_line;
	char new_line;
	char display_mode;
	char exit_mode;
	char speed;
	char stay_time;
	unsigned int data_len;
}__attribute__((__packed__));

struct phy1_head {
	unsigned short dst_addr;
	unsigned short src_addr;
	char reserved[5];
	char display_mode;
	char device_type;
	char protocol_version;
	unsigned short data_len;
} __attribute__((__packed__));


/*
	parameter:
	tty_dev      --device path
	input_stream -- <SIZE#TYPE:DATA:UNIT> eg:<0#4:0.0>,<0#1:0.0:1>,<1#1:1.1:1;2:2.2:1;3:3.3>
	
	notes
	SIZE: 0#-little screen,1#-large screen
	TYPE: 0-8 : DATA_TYPE_TSP ~ DATA_TYPE_PRESS
	DATA: detail data
		  when type=0,1,2,led_size=1,data range:0~9999ug,0~9.99mg;
						  led_size=0,data range:0~9999.9ug,0~99.999mg
	
	UNIT: TSP(0) ~ PM2_5(2) must add UNIT,other data types cannot add unit.
	UNIT: 0 - ug/m3,1 - mg/m3 
	
	return:
	     0 --- OK
	     negative --- error
*/
int led_screen_subtitle(int fd,char *input_stream);

#endif /*__SHOW_LED__*/
