#ifndef __LOOP_QUEUE_H__
#define __LOOP_QUEUE_H__

#define MAX_QUEUE_SIZE 20000
#define ALIGN4(size) ((size + 3) & ~3)

struct node_t {
	char *buff;
	int size;
};

struct loop_queue_t {
	struct node_t nodes[MAX_QUEUE_SIZE];
	int front;
	int rear;
};

int enqueue(struct loop_queue_t *q, char *data, int len);
int dequeue(struct loop_queue_t *q, char *data, int *len);
int dequeue_no_free(struct loop_queue_t *q, char *data, int *len);
int is_queue_empty(struct loop_queue_t *q);

#endif /* end of __LOOP_QUEUE_H__*/
