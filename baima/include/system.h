#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define REBOOT_LOG "/etc/reboot_log"
#define NETWORK_LOG "/etc/network_log"
#define SYSTEM_LOG "/etc/system_log"

enum {
	OFFLINE,
	ONLINE,
};

enum {
	SIM_READY,
	SIM_LOCK,
	SIM_ERROR,
};

enum {
    LINK_DOWN,
    LINK_UP,
};

int system_printf(char *fmt, ...);

void system_reboot(char *msg);

int get_sys_uptime(void);
	
int get_wan_link_state(void);

/*
*Function: run linux command
*Parameter:
*		cmd: linux command to run,eg: ping www.baidu.com
*Return:
*	return the command execute result, eg: 0 if ping success, 1 fails to ping
*/
int run_linux_cmd(char *cmd);

/* return TRUE if process exists, otherwise FALSE */
int is_process_run(char *p);

/* return TRUE if failover is enabled */
int is_failover_enable(void);

void set_online_led(int state);

void set_sim_led(int state);

void set_signal_led(int signal);

int run_background(char * p_cmd);

/*
*	Function: write system reboot log
*     Parameter:
*		msg: the message writed to the file
* 		0 if success to write, -1 if fails to write
*/
int write_reboot_log(char *msg);

/*
*	Function: record network offline or other logs relative to network
*     Parameter:
*		msg: the message writed to the file
* 		0 if success to write, -1 if fails to write
*/
int write_network_log(char *msg);	

/*
*	Function: write system log like process is dead
* 	Parameter:
*		msg: the message writed to the file		
*	Return:
* 		0 if success to write, -1 if fails to write
*/
int write_system_log(char *msg);
#endif /* end of __SYSTEM_H__ */
