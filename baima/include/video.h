#ifndef __TOP_IOT_VIDEO_H__
#define __TOP_IOT_VIDEO_H__

#include <time.h>

void clear_video_subtitle(char *ip, char *username,  char *passwd);
void set_video_subtitle_string(char **strings, int lines, char *ip, char *username, char *passwd);
void set_video_time(char *ip, char *username, char *passwd, struct tm *p);


#endif /* __TOP_IOT_VIDEO_H__ */
