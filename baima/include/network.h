#ifndef __NETWORK_H__
#define __NETWORK_H__
#include "system.h"
#include <libubox/uloop.h>

enum {
	TYPE_UDP,
	TYPE_TCP,
};

typedef void uloop_fd_cb(struct uloop_fd *u, unsigned int events);

typedef void uloop_timer_cb(struct uloop_timeout *timer);
/*
	server_addr: should be IP Address or Domain name
	port: shoud be in range 1~65535
	proto: proto should be TCP or UDP
	timeout: exit if timeout
	return: -1 if connect fail, other connect sucess
*/
int connect_server(char *server_addr, unsigned short port, int proto, int timeout);

int send_data(int fd, char *buff, int len);

int safe_send(int sockfd, void *buf, int len, int flags);

int recv_data(int fd, char *buff, int len);

int set_sock_heartbeat(int sock, int idle, int interval, int cnt);

int connect_unixsock(char *sock_name);	

int send_unixsock_data(int sockfd, char *data, int len);

void init_uloop_fd(struct uloop_fd *ufd, int fd, uloop_fd_cb cb);

void init_uloop_timer(struct uloop_timeout *timer, int timeout, uloop_timer_cb cb);

unsigned short get_sock_port(int fd);

unsigned int get_sock_ip_addr(int fd);

int send_sms(char *phone_num, char *msg);

int send_conf_request(char *request, char *result);

int get_interface_ipaddr(char *ifname);

int get_interface_ipaddr_string(char *ifname, char *ipaddr);

int is_netflow_reach_limit(void);

int is_same_subnet(char *a, char *b);
#endif /*__NETWORK_H__*/
