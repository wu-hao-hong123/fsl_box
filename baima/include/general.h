#ifndef __LIBGENERAL__
#define __LIBGENERAL__

#include <syslog.h>

#define ARRAY_SIZE(x) ((unsigned)(sizeof(x) / sizeof((x)[0])))

#define OPEN_LOG(title) openlog(title, LOG_PID, LOG_USER)
#define DEBUG_WARN(fmt, ...) syslog(LOG_WARNING,"<%s>: "fmt, __func__, ## __VA_ARGS__)
#define DEBUG_INFO(fmt, ...) syslog(LOG_INFO, fmt, ## __VA_ARGS__)

enum {
	FALSE = 0,
	TRUE,
};

enum {
	ERR = -1,
	OK,
};

#endif /* end of __LIBGENERAL__ */
