#ifndef __UTIL_H__
#define __UTIL_H__

unsigned short crc16(unsigned char *puchMsg, unsigned short usDataLen);

/*
*	Function: conver files value to hex value
*	Parameter:
*		Path: file absolute path
*		base: 10 or 16
*	Return: file's value, it return 0 if file not exist, or file start with none-digital number
*/
int ftoi(char *path, int base);

/* 
 * Function: read one line from path
 * Parameter:
 * 		path: file absolute path
 * Return:
 * 		Pointer to a string if it success to read, read max 256 bytes; otherwise it returns NULL
 *
 */
char *fread_line(char *path);

/*
 * Function: empty file
 * Parameter:
 * 		fd: file descriptor
 *	Return:
 *		0, Success to empty, otherwise return -1
 * 		
 */
int empty_file(int fd);

/*
 * 	Function: convert string src to hex dst
 * 	Parameters:
 * 		src: string to be converted
 * 		dst: hex data where to store
 * 	Return:
 * 		Number of bytes to be converted
 */

int asc_to_hex(char *src, char *dst);


int insmod_mod(char *mods);
int rmmod_mod(char *mods);

/*
*	Function: open a named pipe, if not exist, create it
*	Parameter: 
*		path: path of the named pipe, eg: /tmp/event.pipe
*	Return: 
*		file descriptor, it return value is -1, if it fails to open
*/
int open_named_pipe(char *path);

/*
*	Function: write a message to the named pipe
*	Parameter: 
*		fd: file descriptor, should not be negative
*		msg: message to write, should not be NULL and length should be greater 0
*	Return: success to write if return 0, otherwise return -1
*/
int write_named_pipe(int fd, char *msg);

/*
*	Function: send a message to the named pipe
*	Parameter: 
*		path: named pipe path
*		msg: message to write, should not be NULL and length should be greater 0
*	Return: 
*		Success to write if return 0, otherwise return -1
*/
int send_msg_to_named_pipe(char *path, char *msg);

int is_dir_exist(char *path);

/* 
 *	Function: get current system time
 *	Parameter:
 *		str_time: store the system time;format is 'YYYY-MM-DD HH:mm:SS'
 *
 */
void get_system_curr_time(char *str_time);

long long get_system_timestamp(void);

/*
*	Function: strip char on the start and end position
*	Parameter:
* 		str: the string contains c
* 		c: the char needs to strip		
*/
void strip_char(char *str, char c);

#endif /* __UTIL_H__ */
