#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <termios.h>

struct tty_param {
	int  speed;
	char databit;
	char stopbit;
	char parity;
	char flowctl;
};

int transform_speed(int speed);

char *get_speed_string(int speed);
	
int open_serial(char *tty_dev);

int set_serial_parameters(int fd, struct tty_param *param);

int read_serial_data(int fd, char *data, int nsize);

int write_serial_data(int fd, char *data, int nsize);

int read_serial_data_timeout(int fd, char *buff, int len, int timeout, char *dev_name);

#endif /*end of ifndef __SERIAL_H__*/
