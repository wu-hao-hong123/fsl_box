#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
     SerialPort = new serialPort(this);
     SerialPort->init_serial_port(fsl_serial_com7);//fsl_serial_com7

     ui_layout_init();

     //线程处理完的数据传回UI设计这里
     connect(SerialPort->serial_worker, &SerialWorker::newDataReady, [&](const qint8 send_buf_flag){
         qDebug() << "main thread get flag:" << send_buf_flag;

         switch(send_buf_flag)
         {
             case fsl_sned_cmd_get_cco_mac_adress_flag:
             {

             }
             break;
             case fsl_sned_cmd_query_device_nodes_flag:
             {
//                qDebug() << "node_num: " << SerialPort->serial_worker->fsl_query_all_node_num;
                label_get_node_num->setText(QString::number(SerialPort->serial_worker->fsl_query_all_node_num));
             }
             break;
             case fsl_sned_cmd_query_device_info_flag:
             {
                    /*  true: cco    false: sta  */
                   if(SerialPort->serial_worker->cco_device_info.dev_type == true)
                   {
                       if(NULL == parentItem)
                       {
                           parentItem = new QTreeWidgetItem(treeWidget);
                           parentItem->setText(0, "CCO");
                           parentItem->setText(1, SerialPort->serial_worker->cco_device_info.dev_address.toHex(' '));
                           parentItem->setFlags(
                                             Qt::ItemIsUserCheckable
                                           | Qt::ItemIsEnabled
                                           | Qt::ItemIsSelectable
                           );

                           treeWidget->expandItem(parentItem);
                       }
                       else
                       {
                           parentItem->setText(0, "CCO");
                           parentItem->setText(1, SerialPort->serial_worker->cco_device_info.dev_address.toHex(' '));
                       }
                   }
             }
             break;
             case fsl_sned_cmd_query_device_property_flag:
             {
                  qint16 sta_device_count = SerialPort->serial_worker->sta_device_count;
                  if(subItem != NULL)
                  {
                      delete[] subItem;
                  }
                  subItem = new QTreeWidgetItem[sta_device_count];
                  for(qint16 i = 0; i < sta_device_count; i++)
                  {
                        subItem[i].setText(0, "STA");
                        subItem[i].setText(1,
                            SerialPort->serial_worker->sta_device_property_array[i].dev_address.toHex(' '));
                        subItem[i].setFlags(
                                     Qt::ItemIsUserCheckable
                                 |   Qt::ItemIsEnabled
                                 |   Qt::ItemIsSelectable
                         );
                        parentItem->addChild(&subItem[i]);

                  }
             }
             break;
             case fsl_sned_cmd_write_appointed_sta_device_property_flag:
             {

             }
             break;


         }
     });

     connect(SerialPort->serial_timer, &QTimer::timeout, this, [&]()
     {
         SerialPort->serial_isTimeout = true;

         QMessageBox msgBox;
         msgBox.setIcon(QMessageBox::Warning);
         msgBox.setText("串口回复超时!");
         msgBox.setStyleSheet("QMessageBox{background-color: rgba(188, 223, 255,50);\
                              border:2px solid black;\
                             }");
         msgBox.exec();

     });
}

Widget::~Widget()
{

    delete button_test;

    delete pushButton_open_serial;
    delete pushButton_refresh;
    delete Quit_botton;
    delete labelString;
    delete qComboBox_serial;
    delete qComboBox_Baudrate;
    delete label_node_num;
    delete label_MAC_address;
    delete label_device_info;
    delete label_get_node_num;
    delete label_get_MAC_address;
    delete label_get_device_info;
//    delete line_h;
//    delete line_s;


    if(subItem != NULL)
        delete[] subItem;
    if(parentItem != NULL)
        delete parentItem;
    if(treeWidget != NULL)
        delete treeWidget;

    delete slider_value_label;
    delete slider;
    delete checkBox;
    delete label_brightness;
    delete label_voltage;
    delete label_electricity;
    delete label_power;
    delete label_leak_current;     //漏电流
    delete label_power_factor;     //功率因数
    delete label_temperature;
    delete label_qingxie;
    delete label_warning;
    delete label_run_time;
    delete label_electric_quantity;        //累计电量

    delete label_voltage_value;
    delete label_electricity_value;
    delete label_power_value;
    delete label_leak_current_value;       //漏电流
    delete label_power_factor_value;       //功率因数
    delete label_temperature_value;
    delete label_qingxie_value;
    delete label_warning_value;
    delete label_run_time_value;
    delete label_electric_quantity_value;        //累计电量

    delete label_voltage_unit;
    delete label_electricity_unit;
    delete label_power_unit;
    delete label_leak_current_unit;            //漏电流
    delete label_power_factor_unit;            //功率因数
    delete label_temperature_unit;
    delete label_qingxie_unit;
    delete label_run_time_unit;
    delete label_electric_quantity_unit;      //累计电量



    delete page1;
    delete page2;
    delete page3;

    delete m_stackedWidget; // 存放各个页面的容器
    delete m_button1; // 导航栏上的按钮
    delete m_button2;
    delete m_button3;

    delete SerialPort;




}


void Widget::ui_layout_init()
{

#ifdef ARM
    this->resize(800, 480);
#else
    //this->resize(list_screen.at(0)->geometry().width(),list_screen.at(0)->geometry().height());
    this->resize(800, 480);             //调试阶段仍按800 * 480, 和板子对齐
#endif

//    this->setWindowTitle("PLC上位机");
    // 新建页面
    page1 = new QWidget(this);
    page2 = new QWidget(this);
    page3 = new QWidget(this);


    /*init object*/
    pushButton_open_serial = new QPushButton("打开串口", page1);
    pushButton_refresh = new QPushButton("刷新", page1);

    qComboBox_serial = new QComboBox(page1);
    qComboBox_Baudrate = new QComboBox(page1);

//------------------------------------------------------------------------
    pushButton_open_serial->setGeometry(230,10, 80, 30);
    pushButton_refresh->setGeometry(330,10, 80, 30);
    pushButton_refresh->setEnabled(false);

    connect(pushButton_open_serial, SIGNAL(clicked()), this, SLOT(pushButton_open_serial_Clicked()));
    connect(pushButton_refresh, SIGNAL(clicked()), this, SLOT(pushButton_refresh_Clicked()));

//------------------------------------------------------------------------

    labelString = new QLabel(page1);
    labelString->setText("FSL");
    labelString->setGeometry(600, 0, 100, 50);
    labelString->setStyleSheet("font-size:40px;color:black");

    label_node_num = new QLabel(page1);
    label_node_num->setText("节点数量:");
    label_node_num->setGeometry(30, 300, 80, 30);

    label_MAC_address = new QLabel(page1);
    label_MAC_address->setText("MAC地址:");
    label_MAC_address->setGeometry(30, 330, 70, 30);

    label_device_info = new QLabel(page1);
    label_device_info->setText("设备信息:");
    label_device_info->setGeometry(30, 360, 80, 30);

    label_get_node_num = new QLabel(page1);
    label_get_node_num->setGeometry(120, 300, 100, 30);

    label_get_MAC_address = new QLabel(page1);
    label_get_MAC_address->setGeometry(110, 330, 300, 30);

    label_get_device_info = new QLabel(page1);
    label_get_device_info->setGeometry(110, 340, 300, 120);


//------------------------------------------------------------------------
    qComboBox_serial->setGeometry(30, 10, 80, 30);
    qComboBox_serial->addItem("COM7");

    qComboBox_Baudrate->setGeometry(130, 10, 80, 30);
    qComboBox_Baudrate->addItem("115200");
    qComboBox_Baudrate->addItem("9600");
//------------------------------------------------------------------------
//    line_h = new QFrame(page1);
//    line_h->setGeometry(QRect(40, 330, 400, 40));
//    line_h->setFrameShape(QFrame::HLine);
//    line_h->setFrameShadow(QFrame::Sunken);

//    line_s = new QFrame(page1);
//    line_s->setGeometry(QRect(175, 50, 600, 320));
//    line_s->setFrameShape(QFrame::VLine);
//    line_s->setFrameShadow(QFrame::Sunken);
//------------------------------------------------------------------------
    // 节点图
    QList <QString> strList;
    strList<<"节点"<<"单地址"<<"组地址";
    treeWidget = new QTreeWidget(page1);
    treeWidget->setGeometry(30,50,320,250);
    treeWidget->clear();
    treeWidget->setHeaderHidden(false);
    treeWidget->setColumnCount(3);
    treeWidget->setHeaderLabels(strList);
    treeWidget->setFrameStyle(QFrame::StyledPanel);
    treeWidget->setStyleSheet("QTreeView::item { margin: 1px; }");

    /* 信号槽连接 */
    connect(treeWidget,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this, SLOT(treeWidget_Clicked(QTreeWidgetItem* , int)));

//------------------------------------------------------------------------
    // 滑动条信号和槽函数

    slider = new QSlider(page1);
    slider_value_label = new QLabel(page1);

    slider->setFixedWidth(140);     //滑动条长度
    slider->setRange(0, 100);
    slider->setSingleStep(1);
    slider->setOrientation(Qt::Horizontal);
    slider->setGeometry(480, 70, 50, 50);
    QString styleSheet =
            "QSlider::sub-page:horizontal {"            /*1.滑动过的槽设计参数*/
            "background: rgb(235,97,0);"
            "border-radius: 0px;"
            "margin-top:8px;"
            "margin-bottom:8px;"
            "}"
            "QSlider::add-page:horizontal {"            /*2.未滑动过的槽设计参数*/
            "background: rgb(255,255, 255);"
            "border: 0px solid #777;"
            "border-radius: 2px;"
            "margin-top:8px;"
            "margin-bottom:8px;"
            "}"
            "QSlider::handle:horizontal {"          /*3.滑块设计参数*/
            "background: rgb(255,153,102);"
            "border: 1px solid rgb(255,153,102);"
            "width: 20px;"
            "height:20px;"
            "border-radius: 7px;"
            "margin-top:2px;"
            "margin-bottom:2px;"
            "}"
            "QSlider::handle:horizontal:hover {"
            "background: rgb(255,128,6);"
            "border: 1px solid rgba(102,102,102,102);"
            "border-radius: 7px;"
            "}"
            "QSlider::sub-page:horizontal:disabled {"
            "background: #bbb;"
            "border-color: #999;"
            "}"
            "QSlider::add-page:horizontal:disabled {"
            "background: #eee;"
            "border-color: #999;"
            "}"
            "QSlider::handle:horizontal:disabled {"
            "background: #eee;"
            "border: 1px solid #aaa;"
            "border-radius: 4px;"
            "}";

    slider->setStyleSheet(styleSheet);
//    slider->hide();
    slider_value_label->setGeometry(640, 65, 50, 50);
    slider_value_label->setText("0");

    connect(slider, &QSlider::valueChanged, [=](int value) {
        slider_value_label->setText(QString::number(value));
    });
    connect(slider, &QSlider::sliderReleased, this, [=]() {
        qDebug() << slider->value() * 100;
//        if(checkBox->isChecked())
//        {
//            SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property,
//                                                     SerialPort->serial_worker->sta_device_property_array[childIndex].mac_address,
//                                                     true,
//                                                     slider->value() * 100);

//        }
//        else
//        {
//            SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property,
//                                                     SerialPort->serial_worker->sta_device_property_array[childIndex].mac_address,
//                                                     false,
//                                                     slider->value() * 100);
//        }
    });
//------------------------------------------------------------------------
    //属性列表
    checkBox = new QCheckBox("开关", page1);
    checkBox->setStyleSheet("QCheckBox::indicator {width: 20px;height: 20px;}");
    checkBox->setChecked(false);
    checkBox->setGeometry(430, 40, 100, 50);
    checkBox->hide();
    connect(checkBox, &QCheckBox::stateChanged, [=](qint8 state){
        if (state == Qt::Checked)
        {
//            qDebug() << "复选框已选中";
            if(childIndex >= 0)
            {
                SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property,
                                                         SerialPort->serial_worker->sta_device_property_array[childIndex].mac_address,
                                                         true,
                                                         -1);
            }
        }
        else
        {
//            qDebug() << "复选框未选中";
            if(childIndex >= 0)
            {
                SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property,
                                                         SerialPort->serial_worker->sta_device_property_array[childIndex].mac_address,
                                                         false,
                                                         -1);
            }
        }
    });

    ui_label_display();

//------------------------------------------------------------------------

    // 新建容器
    m_stackedWidget = new QStackedWidget(this);
    m_button1 = new QPushButton("节点图", this);
    m_button2 = new QPushButton("Page 2", this);
    m_button3 = new QPushButton("Page 3", this);

    Quit_botton = new QToolButton(this);
    QStyle *style = QApplication::style();
    QIcon icon = style->standardIcon(QStyle::SP_TitleBarCloseButton);
    Quit_botton->setIcon(icon);
    Quit_botton->setText("退出程序");
    Quit_botton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    button_test = new QPushButton(this);
    button_test->setGeometry(200,200,100,50);
    button_test->setText("test");

    // 添加页面到容器
    m_stackedWidget->addWidget(page1);
    m_stackedWidget->addWidget(page2);
    m_stackedWidget->addWidget(page3);

    QVBoxLayout *vLayout = new QVBoxLayout(this);
    vLayout->addWidget(m_button1);
    vLayout->addWidget(m_button2);
    vLayout->addWidget(m_button3);
    vLayout->addWidget(Quit_botton);
    vLayout->addWidget(button_test);
    vLayout->addStretch(1); // 添加弹簧，使导航栏按钮垂直方向居中
    vLayout->setSpacing(15); // 设置控件间距

    QWidget *navWidget = new QWidget;
    navWidget->setLayout(vLayout);

    QHBoxLayout *hLayout = new QHBoxLayout(this);
    hLayout->addWidget(navWidget);
    hLayout->addWidget(m_stackedWidget);

    // 导航栏按钮点击事件
    connect(m_button1, &QPushButton::clicked, [=]() {
        m_stackedWidget->setCurrentIndex(0);
    });

    connect(m_button2, &QPushButton::clicked, [=]() {
        m_stackedWidget->setCurrentIndex(1);
    });

    connect(m_button3, &QPushButton::clicked, [=]() {
        m_stackedWidget->setCurrentIndex(2);
    });

    connect(Quit_botton, SIGNAL(clicked()), this, SLOT(quit_Clicked()));
    connect(button_test, SIGNAL(clicked()), this, SLOT(fsl_pushButton_open_serial_Clicker_get_cco_address()));

//------------------------------------------------------------------------

}

void Widget::label_clear()
{
//    int topLevelItemCount = treeWidget->topLevelItemCount();
//    for(int i=0; i<topLevelItemCount; i++) {
//        QTreeWidgetItem *item = treeWidget->topLevelItem(i);
//        qDebug() << "i = " << i;
//        item->setText(0, "");
//        item->setText(1, "");
//        item->setText(2, "");
//    }

//    if(parentItem != NULL)
//    {
//        delete parentItem;
//        parentItem = NULL;
//    }
//    if(subItem != NULL)
//    {
//        delete[] subItem;
//        subItem = NULL;
//    }

   checkBox->hide();
   slider->hide();
   label_brightness->clear();
   slider_value_label->clear();
   label_get_node_num->clear();
   label_get_MAC_address->clear();
   label_get_device_info->clear();
   label_voltage_value->clear();
   label_electricity_value->clear();
   label_power_value->clear();
   label_leak_current_value->clear();
   label_power_factor_value->clear();
   label_temperature_value->clear();
   label_qingxie_value->clear();
   label_warning_value->clear();
   label_run_time_value->clear();
   label_electric_quantity_value->clear();
}

void Widget::ui_label_display()
{
    label_brightness = new QLabel("亮度", page1);
    label_voltage = new QLabel("电压", page1);
    label_electricity = new QLabel("电流", page1);
    label_power = new QLabel("功率", page1);
    label_leak_current = new QLabel("漏电流", page1);       //漏电流
    label_power_factor = new QLabel("功率因数", page1);       //功率因数
    label_temperature = new QLabel("温度", page1);
    label_qingxie = new QLabel("倾斜角度", page1);
    label_warning = new QLabel("警告", page1);
    label_run_time = new QLabel("运行时间", page1);
    label_electric_quantity = new QLabel("累计电量", page1);      //电量累计

    label_brightness->setGeometry(430, 70, 50, 50);
    label_voltage->setGeometry(430, 100, 50, 50);
    label_electricity->setGeometry(430, 130, 50, 50);
    label_power->setGeometry(430, 160, 50, 50);
    label_leak_current->setGeometry(430, 190, 50, 50);
    label_power_factor->setGeometry(430, 220, 70, 50);
    label_temperature->setGeometry(430, 250, 50, 50);
    label_qingxie->setGeometry(430, 280, 70, 50);
    label_warning->setGeometry(430, 310, 50, 50);
    label_run_time->setGeometry(430, 340, 70, 50);
    label_electric_quantity->setGeometry(430, 370, 70, 50);

    label_voltage_value = new QLabel(page1);
    label_electricity_value = new QLabel(page1);
    label_power_value = new QLabel(page1);
    label_leak_current_value  = new QLabel(page1);
    label_power_factor_value  = new QLabel(page1);
    label_temperature_value = new QLabel(page1);
    label_qingxie_value = new QLabel(page1);
    label_warning_value = new QLabel(page1);
    label_run_time_value = new QLabel(page1);
    label_electric_quantity_value = new QLabel(page1);

    label_voltage_value->setGeometry(520, 100, 50, 50);
    label_electricity_value->setGeometry(520, 130, 50, 50);
    label_power_value->setGeometry(520, 160, 50, 50);
    label_leak_current_value->setGeometry(520, 190, 50, 50);
    label_power_factor_value->setGeometry(520, 220, 70, 50);
    label_temperature_value->setGeometry(520, 250, 50, 50);
    label_qingxie_value->setGeometry(520, 280, 70, 50);
    label_warning_value->setGeometry(520, 310, 50, 50);
    label_run_time_value->setGeometry(520, 340, 70, 50);
    label_electric_quantity_value->setGeometry(520, 370, 70, 50);

    label_voltage_unit = new QLabel("V",page1);
    label_electricity_unit = new QLabel("A", page1);
    label_power_unit = new QLabel("W", page1);
    label_leak_current_unit  = new QLabel("mA", page1);
    label_power_factor_unit  = new QLabel("%", page1);
    label_temperature_unit = new QLabel("°C", page1);
    label_qingxie_unit = new QLabel("°", page1);
    label_run_time_unit = new QLabel("秒", page1);
    label_electric_quantity_unit = new QLabel("kWh", page1);

    label_voltage_unit->setGeometry(600, 100, 50, 50);
    label_electricity_unit->setGeometry(600, 130, 50, 50);
    label_power_unit->setGeometry(600, 160, 50, 50);
    label_leak_current_unit->setGeometry(600, 190, 50, 50);
    label_power_factor_unit->setGeometry(600, 220, 70, 50);
    label_temperature_unit->setGeometry(600, 250, 50, 50);
    label_qingxie_unit->setGeometry(600, 280, 70, 50);
    label_run_time_unit->setGeometry(600, 340, 70, 50);
    label_electric_quantity_unit->setGeometry(600, 370, 70, 50);


}

void Widget::treeWidget_Clicked(QTreeWidgetItem *item, int)
{
        QByteArray data_buf;
        qint32 value = 0;
        double result = 0;

        if(item->parent() == NULL)
        {
//            qDebug() << "父节点";
            childIndex = -1;
            label_get_MAC_address->setText(SerialPort->serial_worker->cco_device_info.mac_address.toHex(' '));
            qint32 len = SerialPort->serial_worker->cco_device_info.string_dev_info.length();
            QString string_front = SerialPort->serial_worker->cco_device_info.string_dev_info.mid(0, 32);
            QString string_middle_1 = SerialPort->serial_worker->cco_device_info.string_dev_info.mid(32, 27);
            QString string_middle_2 = SerialPort->serial_worker->cco_device_info.string_dev_info.mid(32+27, 18);
            QString string_back =  SerialPort->serial_worker->cco_device_info.string_dev_info.mid(32+27+18, len - 32 - 27 - 18);
            label_get_device_info->setText(string_front + "\n" + string_middle_1 + "\n" + string_middle_2 + "\n" + string_back);

            checkBox->hide();
            slider->hide();
            label_brightness->clear();
            slider_value_label->clear();
            label_voltage_value->setText("");
            label_electricity_value->setText("");
            label_power_value->setText("");
            label_leak_current_value->setText("");
            label_power_factor_value->setText("");
            label_temperature_value->setText("");
            label_qingxie_value->setText("");
            label_warning_value->setText("");
            label_run_time_value->setText("");
            label_electric_quantity_value->setText("");

        }
        else
        {
            childIndex = item->parent()->indexOfChild(item);
//          qDebug() << "子节点索引：" << childIndex;
            label_get_MAC_address->setText(SerialPort->serial_worker->sta_device_property_array[childIndex].mac_address.toHex(' '));
            label_get_device_info->setText("无");

            /* 显示开关状态 */
            checkBox->show();
            slider->show();
            if(SerialPort->serial_worker->sta_device_property_array[childIndex].light_switch == true)
                checkBox->setChecked(true);
            else
                checkBox->setChecked(false);

            /* 显示电压 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].voltage_value;
           QDataStream stream(data_buf);
           stream.setByteOrder(QDataStream::LittleEndian);
           stream >> value;
           result = value * (0.1);
           label_voltage_value->setText(QString::number(result));
           data_buf.clear();

           /* 显示电流 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].electricity_value;
           QDataStream electricity_stream(data_buf);
           electricity_stream.setByteOrder(QDataStream::LittleEndian);
           electricity_stream >> value;
           result = value * (0.001);
           label_electricity_value->setText(QString::number(result));
           data_buf.clear();

           /* 功率 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].power_value;
           QDataStream power_stream(data_buf);
           power_stream.setByteOrder(QDataStream::LittleEndian);
           power_stream >> value;
           result = value * (0.001);
           label_power_value->setText(QString::number(result));
           data_buf.clear();


           /* 漏电流 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].leak_current_value;
           QDataStream leak_current_stream(data_buf);
           leak_current_stream.setByteOrder(QDataStream::LittleEndian);
           leak_current_stream >> value;
           label_leak_current_value->setText(QString::number(value));
           data_buf.clear();


           /* 功率因数 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].power_factor_value;
           QDataStream power_factor_stream(data_buf);
           power_factor_stream.setByteOrder(QDataStream::LittleEndian);
           power_factor_stream >> value;
           label_power_factor_value->setText(QString::number(value));
           data_buf.clear();


           /* 温度 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].temperature_value;
           QDataStream temperature_stream(data_buf);
           temperature_stream.setByteOrder(QDataStream::LittleEndian);
           temperature_stream >> value;
//           result = value * (0.1);
           label_temperature_value->setText(QString::number(value));
           data_buf.clear();


           /* 倾斜角度 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].qingxie_value;
           QDataStream qingxie_stream(data_buf);
           qingxie_stream.setByteOrder(QDataStream::LittleEndian);
           qingxie_stream >> value;
           label_qingxie_value->setText(QString::number(value));
           data_buf.clear();


           /* 警告 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].warning_value;
           QDataStream warning_stream(data_buf);
           warning_stream.setByteOrder(QDataStream::LittleEndian);
           warning_stream >> value;
//         qDebug() << "[DEBUG]  electricity_value" << result;
           switch(value)
           {
               case 0:
                    label_warning_value->setText("灯故障");
                    break;
               case 1:
                    label_warning_value->setText("倾斜");
                    break;
               case 2:
                    label_warning_value->setText("过温");
                    break;
               case 3:
                    label_warning_value->setText("漏电");
                    break;
               case 4:
                    label_warning_value->setText("过载");
                    break;
               case 5:
                    label_warning_value->setText("过压");
                    break;
               case 6:
                    label_warning_value->setText("欠压");
                    break;
               case 7:
                    label_warning_value->setText("过流");
                    break;
           }

           data_buf.clear();


           /* 运行时间 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].run_time_value;
           QDataStream run_time_stream(data_buf);
           run_time_stream.setByteOrder(QDataStream::LittleEndian);
           run_time_stream >> value;
           label_run_time_value->setText(QString::number(value));
           data_buf.clear();


           /* 累计电量 */
           data_buf = SerialPort->serial_worker->sta_device_property_array[childIndex].electric_quantity_value;
           QDataStream electric_quantity_stream(data_buf);
           electric_quantity_stream.setByteOrder(QDataStream::LittleEndian);
           electric_quantity_stream >> value;
           result = value * (0.0001);
           label_electric_quantity_value->setText(QString::number(result));
           data_buf.clear();

        }
}

void Widget::fsl_pushButton_open_serial_Clicker_get_cco_address()
{
    if(m_count)
    {
        SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property, QByteArray::fromHex("0000C0A80170"), true, 2707);
        m_count = false;
    }
    else
    {
        SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_write_sta_device_property, QByteArray::fromHex("0000C0A80170"), false, 2707);
        m_count = true;
    }
}

//槽函数
void Widget::pushButton_open_serial_Clicked()
{
    if(pushButton_open_serial->text() == "打开串口")
    {
        if(SerialPort->serial_com->open(QIODevice::ReadWrite))
        {
            pushButton_open_serial->setText("关闭串口");
            pushButton_refresh->setEnabled(true);

            SerialPort->fsl_handle_send_uart_default_cmd(fsl_send_default_common_Query_device_nodes);
            SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_query_device_info_func_code,QByteArray::fromHex("00"), false, -1);
            SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_query_device_property_func_code,QByteArray::fromHex("00"), false, -1);

        }
        else
        {
            pushButton_open_serial->setText("打开串口");
            pushButton_refresh->setEnabled(false);
        }

    }
    else if(pushButton_open_serial->text() == "关闭串口")
    {
        pushButton_refresh->setEnabled(false);
        pushButton_open_serial->setText("打开串口");
        label_clear();

        SerialPort->serial_com->close();
    }

   // this->fsl_pushButton_open_serial_Clicker_get_cco_address();
}

void Widget::pushButton_refresh_Clicked()
{
    //this->setStyleSheet("QWidget { background-color: rgba(238, 122, 233, 100%); }");
    label_clear();
    SerialPort->fsl_handle_send_uart_default_cmd(fsl_send_default_common_Query_device_nodes);
    SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_query_device_info_func_code,QByteArray::fromHex("00"), false, -1);
    SerialPort->fsl_handle_send_uart_cus_cmd(fsl_send_cco_cus_query_device_property_func_code, QByteArray::fromHex("00"), false, -1);

}

void Widget::quit_Clicked()
{
    QApplication *app;
    app->quit();
}
