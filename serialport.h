#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "head.h"

class SerialWorker : public QObject
{
    Q_OBJECT
public:
    SerialWorker(QObject* parent = nullptr)
        : QObject(parent)
    {}
    ~SerialWorker()
    {
        if(sta_device_property_array != NULL)
        {
            delete[] sta_device_property_array;
        }
    }

public slots:
    void HandleSerialData(QByteArray data);    //qint8 send_buf_flag);

signals:
    void newDataReady(qint8 send_buf_flag);

public:
    qint8 send_cmd_flag = 0;
    qint16 fsl_query_all_node_num = 0;
    CCO_Device_Info cco_device_info;
    STA_Device *sta_device_property_array = NULL;
    qint16 sta_device_count = 0;
    qint8 array_pos = 0;

public:
    qint16 fsl_U8_to_U16(char front, char back);
    string fsl_hexArrayToAsciiString(unsigned char arr[], int len);
    QString byteArrayToHexString(const QByteArray& data);
    qint16 byteArrayToQint16(const QByteArray& data);
    void m_handle(QByteArray data, qint8 send_buf_flag);
    void cus_handle(QByteArray data);
    void default_handle(QByteArray cmd, QByteArray data);
    void fsl_handle_query_device_nodes(QByteArray data);
    void fsl_handle_query_device_info(QByteArray data);
    void fsl_handle_query_device_property(QByteArray data);
    void fsl_handle_query_device_property_result(QByteArray data, STA_Device *sta_device_property_array, qint16 array_pos);


};


class serialPort : public QWidget
{
    Q_OBJECT

public:
    explicit serialPort(QWidget *parent = nullptr);
    ~serialPort();

    bool init_serial_port(QString com);
    qint8 fsl_handle_send_uart_cus_cmd(uint8_t cmd, QByteArray dest_mac, bool light_switch, int brightness);
    qint8 fsl_handle_send_uart_default_cmd(uint8_t cmd);
    void fsl_send_buf_to_uart(unsigned char *data, unsigned int datalen);
    uint8_t fsl_u16Tou8(qint16 data, unsigned char *buf, uint8_t pos, bool crc_verify);
    unsigned short CRC16_CCITT_FALSE(unsigned char *data, unsigned int datalen);

    /*  发送到CCO的指令  */
    void fsl_get_cco_mac_adress();          //读取模组通信地址
    void fsl_query_device_nodes();            //读取拓扑中节点数量
    void fsl_query_device_info_function();           //查询设备信息功能
    void fsl_query_device_property( unsigned char *dest_mac_address, qint32 buf_size);     //查询设备属性指令
    void fsl_write_sta_device_property( QByteArray dest_mac_address, bool device_switch, int brightness);


public slots:
     void serialPortReadyRead();

public:
    QSerialPortInfo info;
    QSerialPort *serial_com;
    QThread *SerialProcessThread;
    SerialWorker *serial_worker;
    QTimer *serial_timer;

    /* 定时器 */
    bool serial_isTimeout = false;

    /* 发送buf协议所需变量定义 */
    unsigned char broadcast_address[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
    qint16 send_buf_seq = 0x0000;
    char send_buf_PLC_master_version_number = 0x00;
    char send_buf_PLC_slave_version_number = 0x00;
    char send_buf_status_code = 0x00;
    qint16 send_buf_dev_addr = 0x0000;
};

/*
class m_serialPortThread : public QThread
{
    Q_OBJECT
public:
    void run() override
        {
            while(true)
            {
                if (serial_com7->waitForReadyRead())
                {
                    QByteArray data = serial_com7->readAll();  //读取所有数据

                    qDebug() << data.toHex(' ');

                    emit recvData(data);
                }
            }
        }
    signals:
        void recvData(QByteArray data);

public:
        QSerialPort *serial_com7;
};
*/

#endif // SERIALPORT_H
